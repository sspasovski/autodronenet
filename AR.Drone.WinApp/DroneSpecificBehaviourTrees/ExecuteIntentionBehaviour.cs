﻿using AR.Drone.Avionics.Objectives;
using AR.Drone.Avionics.Objectives.IntentObtainers;
using BehaviourTrees.Utility;
using System;
using System.Linq;

namespace AR.Drone.WinApp.DroneSpecificBehaviourTrees
{
	public class ExecuteIntentionBehaviour : ObjectiveArbiterBehaviour
	{
		public ExecuteIntentionBehaviour() { }
		public IntentionBuilder[] Intentions { get; set; }
		public float Duration { get; set; }

		protected override void OnFirstInitialize()
		{
			IntentObtainer[] inentionsThatDefineTheObjective = new IntentObtainer[Intentions.Length];
			for (int i = 0; i < Intentions.Length; i++)
			{
				IntentionBuilder intentionBuilder = this.Intentions[i];
				Type intentionType = typeof(IntentObtainer).GetAllDerivedTypesInDomain().First(type => type.Name == intentionBuilder.Name);
				IntentObtainer intentObtainer = CreateIntention(intentionType, intentionBuilder.Value);
				inentionsThatDefineTheObjective[i] = intentObtainer;
			}

			this.Objective = Objective.Create((long)this.Duration, inentionsThatDefineTheObjective);
		}

		private static IntentObtainer CreateIntention(Type intentionType, float value)
		{
			try
			{
				return (IntentObtainer)Activator.CreateInstance(intentionType, new object[] { value });
			}
			catch { }

			try
			{
				return (IntentObtainer)Activator.CreateInstance(intentionType, new object[] { value, 1f });
			}
			catch { }

			try
			{
				return (IntentObtainer)Activator.CreateInstance(intentionType, new object[] { value, 1f, true });
			}
			catch { }

			throw new InvalidOperationException("Intention of type " + intentionType.Name + " cannot be created");
		}
	}
}
