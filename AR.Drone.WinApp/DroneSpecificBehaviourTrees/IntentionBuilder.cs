﻿
namespace AR.Drone.WinApp.DroneSpecificBehaviourTrees
{
	public class IntentionBuilder
	{
		public string Name { get; set; }
		public float Value { get; set; }

		public static IntentionBuilder Create(string name, float value)
		{
			IntentionBuilder builder = new IntentionBuilder();
			builder.Name = name;
			builder.Value = value;
			return builder;
		}
	}
}
