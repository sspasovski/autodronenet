﻿using AR.Drone.Avionics.Objectives;
using BehaviourTrees.Utility;
using System;
using System.Linq;

namespace AR.Drone.WinApp.DroneSpecificBehaviourTrees
{
	public class ExecuteObjectiveBehaviour : ObjectiveArbiterBehaviour
	{
		public ExecuteObjectiveBehaviour() { }
		public string ObjectiveName { get; set; }
		public long Duration { get; set; }

		protected override void OnFirstInitialize()
		{
			Type objectType;
			try
			{
				objectType = typeof(Objective).GetAllDerivedTypesInDomain().First(type => type.Name == this.ObjectiveName);
			}
			catch (InvalidOperationException exception)
			{
				throw new InvalidOperationException(
					"The object name doesn't match any of the existing Objective derived types",
					exception);
			}

			this.Objective = (Objective)Activator.CreateInstance(objectType, new object[] { this.Duration });
		}
	}
}
