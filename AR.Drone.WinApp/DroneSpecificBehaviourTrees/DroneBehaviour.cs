﻿using AR.Drone.Avionics;
using BehaviourTrees;
using System;

namespace AR.Drone.WinApp.DroneSpecificBehaviourTrees
{
	public abstract class DroneBehaviour : Behaviour
	{
		protected DroneAgent droneAgent;

		public override void SetAgent(object agent)
		{
			if (agent == null)
			{
				throw new ArgumentNullException("agent");
			}

			if (!(agent is DroneAgent))
			{
				throw new ArgumentException(
					"the agent is not Drone agent, and is set as agent of DroneBehaviour",
					"agent");
			}

			this.droneAgent = agent as DroneAgent;
			this.agent = agent;
		}
	}
}
