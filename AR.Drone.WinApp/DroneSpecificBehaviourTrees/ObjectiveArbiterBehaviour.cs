﻿using AR.Drone.Avionics.Objectives;
using BehaviourTrees;

namespace AR.Drone.WinApp.DroneSpecificBehaviourTrees
{
	public abstract class ObjectiveArbiterBehaviour : DroneBehaviour
	{
		protected Objective Objective;

		protected override Status Update()
		{
			if (this.Objective.Obtained

				// these two should be with Status Failure probably
				|| this.Objective.Empty || this.Objective.IsExpired)
			{
				return Status.Succeeded;
			}

			this.droneAgent.LastObjective = Objective;
			return Status.Running;
		}
	}
}