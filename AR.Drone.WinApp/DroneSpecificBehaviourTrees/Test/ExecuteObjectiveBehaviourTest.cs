﻿using AR.Drone.Avionics;
using AR.Drone.Client;
using AR.Drone.Data.Navigation;
using BehaviourTrees;
using BehaviourTrees.Math;
using NUnit.Framework;
using System;

namespace AR.Drone.WinApp.DroneSpecificBehaviourTrees.Test
{
	[TestFixture]
	class BehaviourTest
	{
		[Test]
		public void SrializeDeserialze_InstanceOfDoObjectiveBehaviour_Successful()
		{
			var behaviour = new ExecuteObjectiveBehaviour
			{
				Duration = 1000,
				ObjectiveName = "Land"
			};
			string path = @"D:/DoObjectiveBehaviour.xml";
			behaviour.SerialzeToXML(path);
			var newtree = BehaviourBuilder.DeserializeFromXml(path);
			Assert.AreEqual(behaviour.StatelessString(), newtree.StatelessString());
		}

		[Test]
		public void SrializeDeserialze_InstanceOfDoIntentionsBehaviour_Successful()
		{
			var behaviour = new ExecuteIntentionBehaviour
			{
				Duration = 1000,
				Intentions = new[]{
					new IntentionBuilder
					{
						Value = 123,
						Name = "Heading",
					}
				}
			};
			string path = @"D:/DoIntentionsBehaviour.xml";
			behaviour.SerialzeToXML(path);
			var newtree = BehaviourBuilder.DeserializeFromXml(path);
			Assert.AreEqual(behaviour.StatelessString(), newtree.StatelessString());
		}


		[Test]
		public void GetProperty_NumericCondtionFromDroneAgent_TakesTheProperty()
		{
			NumericCondition condition = new NumericCondition
			{
				FieldToCompare = "Five",
				Comparator = NumericComparator.Equal,
				ValueToCompare = 6
			};
			DroneAgent agent = new MockDroneAgent(new DroneClient());
			NavigationData data = new NavigationData();
			data.State = NavigationState.Landed;
			//agent.DroneClient.NavigationData = data;
			Status returnStatus = new BehaviourManager<DroneAgent>(agent, condition).TickBehaviour();
			Assert.AreEqual(returnStatus, Status.Failed);
		}


		[Test]
		public void GetProperty_FlaggedEnumFromDroneAgent_TakesTheProperty()
		{
			EnumCondition condition = new EnumCondition
			{
				FieldToCompare = "MockNavigationData.State",
				Comparator = EnumComparator.HasFlags,
				ValueToCompare = "Flying"
			};
			MockDroneAgent agent = new MockDroneAgent(new DroneClient());
			Status returnStatus = new BehaviourManager<DroneAgent>(agent, condition).TickBehaviour();
			Assert.AreEqual(returnStatus, Status.Succeeded);
		}

		[Test]
		public void FirstTick_DoObjectiveBehaviour_CreatesTheObjective()
		{
			var behaviour = new ExecuteObjectiveBehaviour
			{
				Duration = 1000,
				ObjectiveName = "Land",
			};
			Status returnStatus = new BehaviourManager<DroneAgent>(new DroneAgent(new DroneClient()), behaviour).TickBehaviour();
			Assert.AreEqual(returnStatus, Status.Running);
		}

		[Test]
		public void FirstTick_DoIntentionsBehaviour_CreatesTheObjective()
		{
			var behaviour = new ExecuteIntentionBehaviour
			{
				Duration = 1000,
				Intentions = new[]{
					IntentionBuilder.Create("Heading", 123)
				}
			};
			Status returnStatus = new BehaviourManager<DroneAgent>(new DroneAgent(new DroneClient()), behaviour).TickBehaviour();
			Assert.AreEqual(returnStatus, Status.Running);
		}

		[Test]
		[ExpectedException(typeof(InvalidOperationException))]
		public void FirstTick_DoObjectiveBehaviourOnNoSuchObjective_ThrowsNoSuchObjectiveException()
		{
			var behaviour = new ExecuteObjectiveBehaviour
			{
				Duration = 1000,
				ObjectiveName = "NoSuchObjective",
			};
			Status returnStatus = new BehaviourManager<DroneAgent>(new DroneAgent(new DroneClient()), behaviour).TickBehaviour();
			Assert.AreEqual(returnStatus, Status.Running);
		}

		private class MockDroneAgent : DroneAgent
		{

			public double Five { get; set; }
			public NavigationData MockNavigationData { get; set; }
			public MockDroneAgent(DroneClient droneClient)
				: base(droneClient)
			{
				this.Five = 5;
				MockNavigationData = new NavigationData();
				this.MockNavigationData.State = NavigationState.Flying;
			}
		}
	}
}
