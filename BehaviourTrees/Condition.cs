﻿
namespace BehaviourTrees
{
	public abstract class Condition : Behaviour
	{
		protected override Status Update()
		{
			return this.CheckCondition()
				? Status.Succeeded
				: Status.Failed;
		}

		protected abstract bool CheckCondition();
	}
}
