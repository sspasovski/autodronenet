﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BehaviourTrees
{
	public abstract class Composite : Behaviour
	{
		public List<Behaviour> Children { get; private set; }
		protected int currentChildIndex;
		protected IBehaviour currentChild;

		public Composite()
		{
			Children = new List<Behaviour>();
		}

		public Composite(params Behaviour[] initialChildren)
		{
			Children = new List<Behaviour>(initialChildren);
		}

		/// <summary>
		/// Returns a string that represents the current object.
		/// </summary>
		/// <returns>
		/// A string that represents the current object.
		/// </returns>
		public override string ToString()
		{
			return this.GetType().Name + "[" + Environment.NewLine
				+ this.Children.Select(x => x.ToString()).Aggregate((a, x) => a + "," + Environment.NewLine + x)
				+ Environment.NewLine + "]";
		}

		/// <summary>
		/// Returns a string that represents the current object, without the states lin which he could be.
		/// </summary>
		/// <returns>
		/// A string that represents the current object, without the states in which he could be.
		/// </returns>
		public override string StatelessString()
		{
			return this.GetType().Name + "[" + Environment.NewLine
				+ this.Children.Select(x => x.StatelessString()).Aggregate((a, x) => a + "," + Environment.NewLine + x)
				+ Environment.NewLine + "]";
		}

		protected override void Initialize()
		{
			this.currentChildIndex = 0;
			this.currentChild = null;
		}

		public override void SetAgent(object agent)
		{
			base.SetAgent(agent);
			foreach (IBehaviour behaviour in Children)
			{
				behaviour.SetAgent(this.agent);
			}
		}
	}
}
