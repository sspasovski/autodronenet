﻿
using System;

namespace BehaviourTrees
{
	public class Parallel : Composite
	{
		public Parallel()
		{
		}

		public Parallel(params Behaviour[] children)
			: base(children)
		{
		}

		protected override Status Update()
		{
			bool atLeastOneChildSucceeded = false;
			foreach (IBehaviour child in Children)
			{
				child.Tick();
				switch (child.GetLastStatus())
				{
					case Status.NotInitialized:
						throw new InvalidOperationException("Behaviour shouldn't return not initialized on Tick");

					case Status.Running:
						break;

					case Status.Failed:
						return Status.Failed;

					case Status.Succeeded:
						atLeastOneChildSucceeded = true;
						break;

					default:
						throw new InvalidOperationException("Status not present");
				}
			}

			if (atLeastOneChildSucceeded)
			{
				return Status.Succeeded;
			}

			return Status.Running;
		}
	}
}
