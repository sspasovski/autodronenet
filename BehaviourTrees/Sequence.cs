﻿
using System;

namespace BehaviourTrees
{
	public class Sequence : Composite
	{
		public Sequence()
		{
		}

		public Sequence(params Behaviour[] children)
			: base(children)
		{
		}

		protected override Status Update()
		{
			while (this.currentChildIndex < this.Children.Count)
			{
				this.currentChild = this.Children[currentChildIndex];
				this.currentChild.Tick();
				switch (this.currentChild.GetLastStatus())
				{
					case Status.NotInitialized:
						throw new InvalidOperationException("Behaviour shouldn't return not initialized on Tick");

					case Status.Running:
						return Status.Running;

					case Status.Failed:
						return Status.Failed;

					case Status.Succeeded:
						if (currentChildIndex == this.Children.Count - 1)
						{
							return Status.Succeeded;
						}
						else
						{
							currentChildIndex++;
							break;
						}

					default:
						throw new InvalidOperationException("Status not present");
				}
			}

			return Status.Failed;
		}
	}
}
