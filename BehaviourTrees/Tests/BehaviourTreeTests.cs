﻿using BehaviourTrees.Math;
using NUnit.Framework;
using System;

namespace BehaviourTrees
{
	[TestFixture]
	public class BehaviourTreeTests
	{
		[Test]
		public void SerializeDeserializeBehaviour()
		{
			Selector tree = new Selector();
			Selector firstBranch = new Selector();
			Sequence secondBranch = new Sequence();
			firstBranch.Children.Add(new NumericCondition
				{
					FieldToCompare = "PropertyWithValue2",
					Comparator = NumericComparator.Equal,
					ValueToCompare = 2f
				});
			firstBranch.Children.Add(new MockBehaviour());
			secondBranch.Children.Add(new MockBehaviour());
			secondBranch.Children.Add(new MockBehaviour());
			tree.Children.Add(firstBranch);
			tree.Children.Add(secondBranch);
			new BehaviourManager<MockAgent>(new MockAgent(), tree).TickBehaviour();
			string xmlTreePath = @"D:\tree.xml";
			tree.SerialzeToXML(xmlTreePath);
			var newtree = BehaviourBuilder.DeserializeFromXml(xmlTreePath);
			Assert.AreEqual(tree.StatelessString(), newtree.StatelessString());
		}

		[Test]
		public void TickSequence_AllChildrenSucceed_Succed()
		{
			// Arrange
			Sequence tree = new Sequence();
			tree.Children.Add(new MockBehaviour { ReturnStatus = Status.Succeeded });
			tree.Children.Add(new MockBehaviour { ReturnStatus = Status.Succeeded });
			tree.Children.Add(new MockBehaviour { ReturnStatus = Status.Succeeded });
			tree.Children.Add(new MockBehaviour { ReturnStatus = Status.Succeeded });

			// Act
			Status treeStatusAfterTick = (tree as IBehaviour).Tick();

			// Assert
			Assert.That(treeStatusAfterTick, Is.EqualTo(Status.Succeeded), tree.ToString());
		}

		[Test]
		public void TickSequence_AllChildrenButLastSucceedLastRunning_Running()
		{
			// Arrange
			Sequence tree = new Sequence();
			tree.Children.Add(new MockBehaviour { ReturnStatus = Status.Succeeded });
			tree.Children.Add(new MockBehaviour { ReturnStatus = Status.Succeeded });
			tree.Children.Add(new MockBehaviour { ReturnStatus = Status.Succeeded });
			tree.Children.Add(new MockBehaviour { ReturnStatus = Status.Running });

			// Act
			Status treeStatusAfterTick = (tree as IBehaviour).Tick();

			// Assert
			Assert.That(treeStatusAfterTick, Is.EqualTo(Status.Running), tree.ToString());
		}

		[Test]
		public void TickSelector_ThreeChildrenSecondRunningOtherFail_Running()
		{
			// Arrange
			Selector tree = new Selector();
			tree.Children.Add(new MockBehaviour { ReturnStatus = Status.Failed });
			tree.Children.Add(new MockBehaviour { ReturnStatus = Status.Running });
			tree.Children.Add(new MockBehaviour { ReturnStatus = Status.Failed });

			// Act
			Status treeStatusAfterTick = (tree as IBehaviour).Tick();

			// Assert
			Assert.That(treeStatusAfterTick, Is.EqualTo(Status.Running), tree.ToString());

		}

		[Test]
		public void TickSelector_TwoChildrenThatSucceed_Suceeds()
		{
			// Arrange
			Selector treeSelector = new Selector();
			Sequence firstBranchSequence = new Sequence();
			Selector secondBranchSelector = new Selector();
			firstBranchSequence.Children.Add(new MockBehaviour { ReturnStatus = Status.Succeeded });
			firstBranchSequence.Children.Add(new MockBehaviour { ReturnStatus = Status.Failed });
			secondBranchSelector.Children.Add(new MockBehaviour { ReturnStatus = Status.Failed });
			secondBranchSelector.Children.Add(new MockBehaviour { ReturnStatus = Status.Running });
			treeSelector.Children.Add(firstBranchSequence);
			treeSelector.Children.Add(secondBranchSelector);

			// Act
			Status treeStatusAfterTick = (treeSelector as IBehaviour).Tick();

			// Assert
			Assert.That(treeStatusAfterTick, Is.EqualTo(Status.Running), treeSelector.ToString());
		}


		[Test]
		public void TickNumericCondition_ChecksTheValueOfAgentFieldForEqualityWhenTheyAreEqual_Succeeds()
		{
			var condition = new NumericCondition
			{
				FieldToCompare = "PropertyWithValue2",
				Comparator = NumericComparator.Equal,
				ValueToCompare = 2
			};

			Status status = new BehaviourManager<MockAgent>(new MockAgent(), condition).TickBehaviour();

			Assert.That(status, Is.EqualTo(Status.Succeeded));
		}

		[Test]
		public void TickNumericCondition_ChecksTheValueOfAgentFieldForGreaterThenWhenIsLesser_Fails()
		{
			var condition = new NumericCondition
			{
				FieldToCompare = "PropertyWithValue2",
				Comparator = NumericComparator.GreatherOrEqualThan,
				ValueToCompare = -2.5
			};

			Status status = new BehaviourManager<MockAgent>(new MockAgent(), condition).TickBehaviour();

			Assert.That(status, Is.EqualTo(Status.Succeeded));
		}

		[Test]
		public void TickNumericCondition_ChecksTheValueOfAgentFieldWithSeveralLayersForEquality_Succeeds()
		{
			var condition = new NumericCondition
			{
				FieldToCompare = "ArrayOfTuplesOfTupleOfTupleOfTuple[0].Item2.Item2.Item2.Item1",
				Comparator = NumericComparator.ApproximatlyEqual,
				ValueToCompare = 4
			};

			Status status = new BehaviourManager<MockAgent>(new MockAgent(), condition).TickBehaviour();

			Assert.That(status, Is.EqualTo(Status.Succeeded));
		}

		private class MockAgent
		{
			public double PropertyWithValue2 { get; private set; }
			public Tuple<int, Tuple<int, Tuple<int, Tuple<int, string>>>>[] ArrayOfTuplesOfTupleOfTupleOfTuple { get; private set; }

			public MockAgent()
			{
				PropertyWithValue2 = 2;
				ArrayOfTuplesOfTupleOfTupleOfTuple = new[]
				{
						new Tuple<int, Tuple<int, Tuple<int, Tuple<int, string>>>>
						(
								1,
								new Tuple<int, Tuple<int, Tuple<int, string>>>
								(
									2,
									 new Tuple<int, Tuple<int, string>>
									 (
										 3,
										 new Tuple<int, string>
										 (
											 4,
											 "four"
										 )
									 )
								)
						)
				};
			}
		}

		public class MockBehaviour : Behaviour
		{
			public Status ReturnStatus { get; set; }

			public MockBehaviour()
			{
				ReturnStatus = Status.Running;
			}

			protected override Status Update()
			{
				return this.ReturnStatus;
			}
		}
	}
}
