﻿using BehaviourTrees.Utility;
using System.Xml;
using System.Xml.Serialization;

namespace BehaviourTrees
{
	public class BehaviourBuilder
	{
		public class BehaviourSingletonXMLSerializer
		{
			public static XmlSerializer Instance
			{
				get
				{
					return LazyTreadSafeInsurer.LazyThreadSafeInstance;
				}
			}

			private class LazyTreadSafeInsurer
			{
				static LazyTreadSafeInsurer()
				{
				}

				public static readonly XmlSerializer LazyThreadSafeInstance =
					new XmlSerializer(typeof(Behaviour), typeof(Behaviour).GetAllDerivedTypesInDomain());
			}
		}

		public static Behaviour DeserializeFromXml(string path)
		{
			Behaviour tree = (Behaviour)BehaviourSingletonXMLSerializer.Instance.Deserialize(new XmlTextReader(path));
			return tree;
		}
	}
}
