﻿using System;
using System.Linq;

namespace BehaviourTrees.Utility
{
	public static class TypeExtensions
	{
		public static Type[] GetAllDerivedTypesInDomain(this Type type)
		{
			return AppDomain.CurrentDomain.GetAssemblies().
				Select(x => x.GetTypes())
				// Make union of all types in all assemblies
				.Aggregate((a, x) => a.Union(x).ToArray())
				// which are inherited of behaviour
				.Where(type.IsAssignableFrom)
					.ToArray();
		}
	}
}
