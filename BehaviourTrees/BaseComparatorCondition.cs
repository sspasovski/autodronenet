﻿
using System;
using System.IO;
using System.Reflection;

namespace BehaviourTrees
{
	public abstract class BaseComparatorCondition : Condition
	{
		public enum FieldOrProperty
		{
			Field,
			Property,
		}

		public string FieldToCompare { get; set; }
		public object ValueToCompare { get; set; }
		protected RetrieveFromMemberInfo MemberMetaInfo { get; set; }
		protected class RetrieveFromMemberInfo
		{
			public bool IsLastAccessByIndex { get; set; }
			private FieldInfo FieldInfo { get; set; }
			private PropertyInfo PropertyInfo { get; set; }
			public FieldOrProperty FieldOrProperty { get; set; }
			public object ParentObject { get; set; }
			public int Index { get; set; }

			public object GetValue(object fromObject)
			{
				if (this.FieldOrProperty == FieldOrProperty.Field)
				{
					return this.FieldInfo.GetValue(fromObject);
				}
				else
				{
					return this.PropertyInfo.GetValue(fromObject, null);
				}
			}

			public void RetrieveMemberInfo(object fromObjet, string memberName)
			{
				this.PropertyInfo = fromObjet.GetType().GetProperty(memberName);
				if (this.PropertyInfo != null)
				{
					this.FieldOrProperty = FieldOrProperty.Property;
					this.FieldInfo = null;
				}
				else
				{
					this.FieldInfo = fromObjet.GetType().GetField(memberName);
					if (this.FieldInfo == null)
					{
						throw new InvalidOperationException("there is no field, nor prperty with name " + memberName + " in an instance of " + fromObjet.GetType().Name);
					}
					this.PropertyInfo = null;
					this.FieldOrProperty = FieldOrProperty.Field;
				}
			}
		}

		protected static RetrieveFromMemberInfo GetPropertyMetaInfo(object sourceObject, string propertyName)
		{
			RetrieveFromMemberInfo info = new RetrieveFromMemberInfo();
			if (sourceObject == null)
			{
				throw new ArgumentNullException("sourceObject");
			}

			object objectTraverser = sourceObject;

			// Split property name to parts (propertyName could be hierarchical, like obj.subobj.subobj.property
			string[] propertyNameParts = propertyName.Split('.');

			foreach (string propertyNamePart in propertyNameParts)
			{
				info.ParentObject = objectTraverser;
				if (objectTraverser == null)
				{
					throw new InvalidOperationException(propertyNamePart + "  property is not present in the agent.");
				}

				// propertyNamePart could contain reference to specific 
				// element (by index) inside a collection
				if (!propertyNamePart.Contains("["))
				{
					info.IsLastAccessByIndex = false;
					info.RetrieveMemberInfo(objectTraverser, propertyNamePart);
					objectTraverser = info.GetValue(objectTraverser);
				}
				else
				{
					info.IsLastAccessByIndex = true;

					// get collection name and element index
					int indexStart = propertyNamePart.IndexOf("[", StringComparison.Ordinal) + 1;
					string collectionPropertyName = propertyNamePart.Substring(0, indexStart - 1);
					info.Index = Int32.Parse(propertyNamePart.Substring(indexStart, propertyNamePart.Length - indexStart - 1));
					// get collection object
					//info.PropertyInfo = objectTraverser.GetType().GetProperty(collectionPropertyName);
					info.RetrieveMemberInfo(objectTraverser, collectionPropertyName);

					// matrix accessors still not supported
					info.ParentObject = info.GetValue(objectTraverser);
					objectTraverser = GetValueByIndexFromMember(info.ParentObject, info.Index);
				}
			}

			return info;
		}

		protected static object GetValueByIndexFromMember(object collection, int index)
		{
			System.Collections.IList collectionAsList = collection as System.Collections.IList;
			if (collectionAsList != null)
			{
				return collectionAsList[index];
			}

			throw new InvalidDataException("The collection was accessed by [index] but doesn't implement the IList interface");
		}
	}
}
