﻿namespace BehaviourTrees
{
	public interface IBehaviour
	{
		Status GetLastStatus();
		Status Tick();
		void SetAgent(object agent);
	}
}