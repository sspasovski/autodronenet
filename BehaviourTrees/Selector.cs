﻿

using System;

namespace BehaviourTrees
{
	public class Selector : Composite
	{
		public Selector()
		{
		}

		public Selector(params Behaviour[] children)
			: base(children)
		{
		}

		protected override Status Update()
		{
			if (this.currentChild != null)
			{
				return this.currentChild.Tick();
			}

			while (this.currentChildIndex < this.Children.Count)
			{
				this.currentChild = this.Children[this.currentChildIndex];
				this.currentChild.Tick();
				switch (this.currentChild.GetLastStatus())
				{
					case Status.NotInitialized:
						throw new InvalidOperationException("Behaviour shouldn't return not initialized on Tick");

					case Status.Running:
						return Status.Running;

					case Status.Failed:
						this.currentChildIndex++;
						break;

					case Status.Succeeded:
						return Status.Succeeded;

					default:
						throw new InvalidOperationException("Status not present");
				}
			}

			return Status.Failed;
		}
	}
}
