﻿using System.Text;
using System.Xml;

namespace BehaviourTrees
{
	public static class BehaviourExtensions
	{
		public static void SerialzeToXML(this Behaviour tree, string path)
		{
			XmlWriter xmlWriter = new XmlTextWriter(path, Encoding.UTF8);
			BehaviourBuilder.BehaviourSingletonXMLSerializer.Instance.Serialize(xmlWriter, tree);
			xmlWriter.Close();
		}
	}
}
