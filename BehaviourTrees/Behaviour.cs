﻿namespace BehaviourTrees
{
	public abstract class Behaviour : IBehaviour
	{
		private Status status;
		protected object agent;
		private bool hasFirstInitializePassed;

		protected Behaviour()
		{
			hasFirstInitializePassed = false;
		}

		public Status GetLastStatus()
		{
			return this.status;
		}

		Status IBehaviour.Tick()
		{
			if (this.status != Status.Running)
			{
				this.InitializeTemplate();
			}

			if (Status.Running != (this.status = this.Update()))
			{
				this.Terminate();
			}

			return this.status;
		}

		protected virtual void Terminate()
		{
		}

		protected abstract Status Update();

		private void InitializeTemplate()
		{
			if (!hasFirstInitializePassed)
			{
				this.OnFirstInitialize();
				hasFirstInitializePassed = true;
			}

			Initialize();
		}

		protected virtual void Initialize()
		{
		}

		protected virtual void OnFirstInitialize()
		{
		}

		/// <summary>
		/// Returns a string that represents the current object.
		/// </summary>
		/// <returns>
		/// A string that represents the current object.
		/// </returns>
		public override string ToString()
		{
			return "#status:" + this.GetLastStatus().ToString();
		}

		/// <summary>
		/// Returns a string that represents the current object, without the states in which he could be.
		/// </summary>
		/// <returns>
		/// A string that represents the current object, without the states in which he could be.
		/// </returns>
		public virtual string StatelessString()
		{
			return "#";
		}


		public virtual void SetAgent(object agent)
		{
			this.agent = agent;
		}
	}
}
