﻿
namespace BehaviourTrees
{
	public class BehaviourManager<AgentType>
	{
		public IBehaviour Behaviour { get; private set; }
		public AgentType Agent { get; private set; }

		public BehaviourManager(AgentType agent, IBehaviour behaviour)
		{
			this.Behaviour = behaviour;
			this.Agent = agent;
			this.Behaviour.SetAgent(this.Agent);
		}

		public Status TickBehaviour()
		{
			return this.Behaviour.Tick();
		}
	}
}
