﻿using BehaviourTrees.Math;
using System;

namespace BehaviourTrees
{
	public class EnumCondition : BaseComparatorCondition
	{
		public EnumComparator Comparator { get; set; }
		private Func<Enum, Enum, bool> compareOperator;

		protected override void OnFirstInitialize()
		{
			this.MemberMetaInfo = GetPropertyMetaInfo(this.agent, FieldToCompare);
			this.compareOperator = EnumOperatorExtensions.GetFunctionByOperandType(this.Comparator);
		}

		protected override bool CheckCondition()
		{
			object propertyValue;
			if (this.MemberMetaInfo.IsLastAccessByIndex)
			{
				propertyValue = GetValueByIndexFromMember(
					this.MemberMetaInfo.ParentObject,
					this.MemberMetaInfo.Index);
			}
			else
			{
				propertyValue = MemberMetaInfo.GetValue(this.MemberMetaInfo.ParentObject);
			}

			// here we could do this.Comparator.Operate() but this is for performance reasons better.
			Enum currentPropertyValue;
			try
			{
				currentPropertyValue = (Enum)propertyValue;
			}
			catch (Exception parsingException)
			{
				throw new InvalidOperationException(
					"error when tring to cast " + propertyValue + " as property value",
					parsingException);
			}

			Enum valueToCompare;
			try
			{
				valueToCompare = (Enum)Enum.Parse(propertyValue.GetType(), ValueToCompare.ToString());
			}
			catch (FormatException parsingException)
			{
				throw new InvalidOperationException(
					"error when tring to parse " + ValueToCompare + " as value to compare",
					parsingException);
			}

			return this.compareOperator(currentPropertyValue, valueToCompare);
		}
	}
}
