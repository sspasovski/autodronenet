﻿using System;

namespace BehaviourTrees.Math
{
	public enum EnumComparator
	{
		HasFlags,
		HasAllFlags,
		Equals,
		DoesntHaveFlags
	}

	public static class EnumOperatorExtensions
	{
		public static bool Operate(this EnumComparator numericComparator, Enum firstOperand, Enum secondOperand)
		{
			return GetFunctionByOperandType(numericComparator)(firstOperand, secondOperand);
		}

		public static Func<Enum, Enum, bool> GetFunctionByOperandType(EnumComparator enumComparator)
		{
			switch (enumComparator)
			{
				case EnumComparator.HasFlags:
					return (x1, x2) => x1.HasFlag(x2);
				case EnumComparator.DoesntHaveFlags:
					return (x1, x2) => !x1.HasFlag(x2);
				case EnumComparator.HasAllFlags:
				case EnumComparator.Equals:
					return (x1, x2) => x1.Equals(x2);
				default:
					throw new InvalidOperationException("The requested operator does not exist");
			}
		}
	}
}
