﻿using System;

namespace BehaviourTrees.Math
{
	public enum NumericComparator
	{
		GreatherThan,
		GreatherOrEqualThan,
		Equal,
		LesserOrEqualThan,
		LesserhThan,
		ApproximatlyEqual
	}

	public static class NumericOperatorExtensions
	{
		public static bool Operate(this NumericComparator numericComparator, Double firstOperand, Double secondOperand)
		{
			return GetFunctionByOperandType(numericComparator)(firstOperand, secondOperand);
		}

		public static Func<Double, Double, bool> GetFunctionByOperandType(NumericComparator numericComparator)
		{
			switch (numericComparator)
			{
				case NumericComparator.GreatherThan:
					return (x1, x2) => x1 > x2;
				case NumericComparator.GreatherOrEqualThan:
					return (x1, x2) => x1 >= x2;
				case NumericComparator.Equal:
					return (x1, x2) => x1 == x2;
				case NumericComparator.LesserOrEqualThan:
					return (x1, x2) => x1 <= x2;
				case NumericComparator.LesserhThan:
					return (x1, x2) => x1 < x2;
				case NumericComparator.ApproximatlyEqual:
					return (x1, x2) => (x1 - x2) * (x1 - x2) < Constants.Epsilon * Constants.Epsilon;
				default:
					throw new InvalidOperationException("The requested operator does not exist");
			}
		}
	}
}