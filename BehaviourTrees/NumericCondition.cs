﻿using BehaviourTrees.Math;
using System;

namespace BehaviourTrees
{
	public class NumericCondition : BaseComparatorCondition
	{
		public NumericComparator Comparator { get; set; }
		private Func<Double, Double, bool> compareOperator;

		protected override void OnFirstInitialize()
		{
			this.MemberMetaInfo = GetPropertyMetaInfo(this.agent, FieldToCompare);
			this.compareOperator = NumericOperatorExtensions.GetFunctionByOperandType(this.Comparator);
		}

		protected override bool CheckCondition()
		{
			object propertyValue;
			if (this.MemberMetaInfo.IsLastAccessByIndex)
			{
				propertyValue = GetValueByIndexFromMember(
					this.MemberMetaInfo.ParentObject,
					this.MemberMetaInfo.Index);
			}
			else
			{
				propertyValue = MemberMetaInfo.GetValue(this.MemberMetaInfo.ParentObject);
			}

			// here we could do this.Comparator.Operate() but this is for performance reasons better.
			double currentPropertyValue;
			try
			{
				currentPropertyValue = Double.Parse(propertyValue.ToString());
			}
			catch (Exception parsingException)
			{
				throw new InvalidOperationException(
					"error when tring to cast " + propertyValue + " as property value",
					parsingException);
			}

			double valueToCompare;
			try
			{
				valueToCompare = Double.Parse(ValueToCompare.ToString());
			}
			catch (FormatException parsingException)
			{
				throw new InvalidOperationException(
					"error when tring to parse " + ValueToCompare + " as value to compare",
					parsingException);
			}

			return this.compareOperator(currentPropertyValue, valueToCompare);
		}
	}
}
