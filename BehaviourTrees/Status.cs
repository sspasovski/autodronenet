﻿
namespace BehaviourTrees
{
	public enum Status
	{
		NotInitialized,
		Running,
		Failed,
		Succeeded
	}
}
