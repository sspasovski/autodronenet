﻿using AR.Drone.Avionics.Objectives;
using AR.Drone.Client;

namespace AR.Drone.Avionics
{
	public class DroneAgent
	{
		public DroneClient DroneClient { get; set; }
		public Objective LastObjective { get; set; }

		public DroneAgent(DroneClient droneClient)
		{
			this.DroneClient = droneClient;
		}
	}
}