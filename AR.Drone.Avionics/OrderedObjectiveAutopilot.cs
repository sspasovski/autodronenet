﻿using AR.Drone.Avionics.Objectives;
using AR.Drone.Avionics.Tools.Time;
using AR.Drone.Client;
using AR.Drone.Infrastructure;
using System;
using System.Collections.Concurrent;

namespace AR.Drone.Avionics
{
	/// <summary>
	/// Autopilot is a class that works in a separate thread, handling chains
	/// of user commands queued up for drone control.
	/// </summary>
	/// 
	/// <example>
	///     Typical use:
	///     <code>
	///         DroneClient droneClient = new DroneClient("192.168.1.1");
	///         droneClient.Start();
	///
	///         Autopilot autopilot = new Autopilot(droneClient);
	///         autopilot.BindToClient();
	///         autopilot.Start();
	///         
	///         autopilot.EnqueueObjective(Objective.Create(50, new FlatTrim()));
	///         autopilot.EnqueueObjective(Objective.Create(5000, new Takeoff()));
	///         autopilot.EnqueueObjective(Objective.Create(5000, new Hover()));
	///         autopilot.EnqueueObjective(Objective.Create(5000, new Land()));
	///     </code>
	/// </example>
	public class OrderedObjectiveAutopilot : Autopilot
	{
		private bool _commencingObjective;

		// A queue of objectives waiting in line for their period of time to start contributing to drone control
		protected readonly ConcurrentQueue<Objective> ObjectiveQueue;

		/// <summary>
		/// A callback event, occuring everytime a new objective is being started
		/// </summary>
		public Action<Objective> OnObjectiveStarted { get; set; }

		/// <summary>
		/// A callback event, occuring everytime a an objective had just expired
		/// </summary>
		public Action<Objective> OnObjectiveCompleted { get; set; }

		/// <summary>
		///     Default action of the autopilot, whenever it is out off objectives.
		///     If not specified in a constructor, DefaultObjective will be 'Hover'
		///     by default.
		/// </summary>
		/// 
		/// <remarks>
		///     When the 'OnOutOfObjectives' is called, user may enqueue new
		///     commands for the autopilot, stopping it (the autopilot) from
		///     executing the 'DefaultObjective' objective.
		/// </remarks>
		public readonly Objective DefaultObjective;

		private void ClearOutputQueue()
		{
			ApparatusOutputQueue.Flush();
		}


		/// <summary>
		///     Manages objective's list and provides the caller with current
		///     objective. If objective is expired, the next one in list is
		///     returned, if list is empty, then DefaultObjective is returned.
		/// </summary>
		/// 
		/// <returns>
		///     Current objective, DefaultObjective if ObjectiveQueue is empty
		/// </returns>
		protected override Objective GetObjective()
		{
			Objective objective;
			bool hasObjective;
			while (hasObjective = ObjectiveQueue.TryPeek(out objective))
			{
				if (objective.Empty || objective.IsExpired || objective.Obtained)
				{
					ObjectiveQueue.TryDequeue(out objective);
					if (OnObjectiveCompleted != null) OnObjectiveCompleted.Invoke(objective);
					objective = null;
				}
				else break;
			}

			if (!hasObjective) objective = null;
			else _commencingObjective = true;

			if (objective == null)
			{
				if (_commencingObjective)
				{
					if (OnOutOfObjectives != null)
					{
						OnOutOfObjectives.Invoke();
						_commencingObjective = ObjectiveQueue.TryPeek(out objective); // Check whether user just added a new task
					}
					else _commencingObjective = false;
				}
				if (!_commencingObjective) return DefaultObjective;
			}
			else
			{
				if (!objective.Started)
				{
					objective.Start();
					if (OnObjectiveStarted != null) OnObjectiveStarted.Invoke(objective);
				}
			}

			return objective;
		}



		/// <summary>
		/// Initializes the Autopilot object and associates it with provided DroneClient
		/// </summary>
		/// <param name="aDroneClient">DroneClient object which Autopilot will be controlling</param>
		/// <remarks>Using this constructor will force DeafultObjective to be 'Hover'</remarks>
		public OrderedObjectiveAutopilot(DroneClient aDroneClient)
			: this(aDroneClient, new Hover(Expiration.Never))
		{
			/* Do Nothing */
		}

		/// <summary>
		/// Initializes the Autopilot object and associates it with provided DroneClient and sets DeafultObjective
		/// </summary>
		/// <param name="aDroneClient">DroneClient object which Autopilot will be controlling</param>
		/// <param name="aDefaultObjective">DefaultObjective to execute when ObjectiveQeueue is depleted</param>
		/// <remarks>Using this constructor will force DeafultObjective to be 'Hover'</remarks>
		public OrderedObjectiveAutopilot(DroneClient aDroneClient, Objective aDefaultObjective)
			: base(aDroneClient)
		{
			DefaultObjective = aDefaultObjective;
			ObjectiveQueue = new ConcurrentQueue<Objective>();
			this.OnNonActiveLoop += this.ClearOutputQueue;
		}


		/// <summary>
		/// Add a new objective into autopilot's action queue
		/// </summary>
		/// <param name="aObjective">Objective to be added/queued</param>
		public void EnqueueObjective(Objective aObjective)
		{
			ObjectiveQueue.Enqueue(aObjective);
		}

		/// <summary>
		/// Clears current objective qeueue and adds the provided one into the empty queue
		/// </summary>
		/// <param name="aObjective">Objective to replace the others with</param>
		public void SetObjective(Objective aObjective)
		{
			ClearObjectives();
			ObjectiveQueue.Enqueue(aObjective);
		}

		/// <summary>
		/// Clear all enqueued objectives
		/// </summary>
		public void ClearObjectives()
		{
			bool active = Active;
			Active = false;

			ObjectiveQueue.Flush();

			if (active) Active = true;
		}

		/// <summary>Returns true, if autopilot has any tasks in queue</summary>
		public bool HasObjectives
		{
			get { return !ObjectiveQueue.IsEmpty; }
		}

		/// <summary>Enable autopilot</summary>
		public void Activate()
		{
			Active = true;
		}

		/// <summary>Disable autopilot</summary>
		public void Deactivate()
		{
			Active = false;
		}
	}
}