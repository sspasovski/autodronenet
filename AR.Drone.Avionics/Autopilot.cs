﻿using AR.Drone.Avionics.Objectives;
using AR.Drone.Client;
using AR.Drone.Data.Navigation;
using AR.Drone.Infrastructure;
using System;
using System.Collections.Concurrent;
using System.Threading;

namespace AR.Drone.Avionics
{
	/// <summary>
	/// Autopilot is a class that works in a separate thread, handling chains
	/// </summary>
	public abstract class Autopilot : WorkerBase
	{
		private bool _active;
		private Apparatus.Input _lastInput;

		// A queue of apparatus output (NavigationData) awaiting to be proccessed
		protected readonly ConcurrentQueue<Apparatus.Output> ApparatusOutputQueue;

		/// <summary>
		/// A callback event, occuring when an objective had just expired and there are no new objectives waiting in ObjectiveQueue
		/// </summary>
		public Action OnOutOfObjectives { get; set; }

		/// <summary>
		/// DroneClient which the Autpilot object is bound to
		/// </summary>
		public readonly DroneClient DroneClient;

		/// <summary>
		/// Returns true if autopilot bound to the 'DroneClient.NavigationDataUpdated' event
		/// </summary>
		public bool BoundToClient { get; private set; }

		/// <summary>
		/// A callback event, occuring every time the Autopilot object sends a command to the DroneClient object
		/// </summary>
		public Action<Apparatus.Input> OnCommandSend { get; set; }

		/// <summary>
		/// A callback event, occuring every time the Autopilot object sends a command to the DroneClient object
		/// </summary>
		public Action OnNonActiveLoop { get; set; }

		/// <summary>
		/// Returns the last input to the ArDrone done by the autopilot
		/// </summary>
		public Apparatus.Input LastInput
		{
			get
			{
				lock (this)
				{
					Apparatus.Input value = _lastInput;
					return value;
				}
			}
			protected set
			{
				lock (this)
				{
					_lastInput = value;
				}
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Autopilot"/> class.
		/// </summary>
		/// <param name="aDroneClient"></param>
		protected Autopilot(DroneClient aDroneClient)
		{
			this.DroneClient = aDroneClient;
			this.ApparatusOutputQueue = new ConcurrentQueue<Apparatus.Output>();
			this.BoundToClient = false;
		}


		/// <summary>
		/// Enqueue drone's output data for autopilot handling based on current objective.
		/// </summary>
		/// <param name="aData">Output data to be enqueued</param>
		public void EnqueueOutput(Apparatus.Output aData)
		{
			ApparatusOutputQueue.Enqueue(aData);
		}

		/// <summary>
		/// Bind autopilot to 'DroneClient.NavigationPacketAcquired' action,
		/// allowing autopilot to respond to DroneClient output automaticaly.
		/// </summary>
		public void BindToClient()
		{
			if (!BoundToClient)
			{
				DroneClient.NavigationDataAcquired += NavigationDataAcquired;
				BoundToClient = true;
			}
		}

		/// <summary>
		/// Unbind autopilot from 'DroneClient.NavigationPacketAcquired' action,
		/// ceasing its automatic reponse to DroneClient output
		/// </summary>
		public void UnbindFromClient()
		{
			if (BoundToClient)
			{
				DroneClient.NavigationDataAcquired -= NavigationDataAcquired;
				BoundToClient = false;
			}
		}


		// Event method for 'DroneClient.NavigationPacketAcquired' action
		private void NavigationDataAcquired(NavigationData aPacket)
		{
			if (Active)
			{
				var data = new Apparatus.Output { Navigation = aPacket, LastInput = LastInput };
				EnqueueOutput(data);
			}
		}

		/// <summary>
		/// Returns true if autopilot is enabled
		/// </summary>
		public bool Active
		{
			get
			{
				lock (this)
				{
					return _active;
				}
			}
			set
			{
				lock (this)
				{
					_active = value;
				}
			}
		}


		/// <summary>
		/// Thread looping method resposible for Autopilot to queued new objectives.
		/// </summary>
		protected sealed override void Loop(CancellationToken token)
		{
			while (!token.IsCancellationRequested)
			{
				if (!Active)
				{
					if (this.OnNonActiveLoop != null)
					{
						this.OnNonActiveLoop();
					}

					Thread.Sleep(10);
				}
				else
				{
					Apparatus.Output output;

					if (ApparatusOutputQueue.TryDequeue(out output))
					{
						Objective currentIntent = GetObjective();

						var input = new Apparatus.Input();
						input.Reset();

						currentIntent.Contribute(output, ref input);
						input.Send(DroneClient);

						LastInput = input;
						if (OnCommandSend != null) OnCommandSend.Invoke(input);
					}
					else Thread.Sleep(3);
				}
			}
		}

		protected abstract Objective GetObjective();
	}
}