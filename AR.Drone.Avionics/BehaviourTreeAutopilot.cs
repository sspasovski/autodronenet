﻿using AR.Drone.Avionics.Objectives;
using AR.Drone.Client;
using BehaviourTrees;
using System;

namespace AR.Drone.Avionics
{
	public class BehaviourTreeAutopilot : Autopilot
	{
		private BehaviourManager<DroneAgent> beaviourManager;
		private DroneAgent droneAgent;

		/// <summary>
		/// Initializes a new instance of the <see cref="Autopilot"/> class.
		/// </summary>
		/// <param name="aDroneClient"></param>
		public BehaviourTreeAutopilot(DroneClient aDroneClient, Behaviour behaviour)
			: base(aDroneClient)
		{
			this.droneAgent = new DroneAgent(aDroneClient);
			this.beaviourManager = new BehaviourManager<DroneAgent>(droneAgent, behaviour);
		}

		protected override Objective GetObjective()
		{
			this.beaviourManager.TickBehaviour();
			Objective objective = this.beaviourManager.Agent.LastObjective;
			Console.WriteLine(objective.GetType().Name);
			if (!objective.Started)
			{
				objective.Start();
			}
			return objective;
		}
	}
}
