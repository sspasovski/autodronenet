## AR.Drone the autonomous drone

This project is build to test the ability to use behavior trees on AR.Drone.

Built over the original [Ruslans AR.Drone project](https://github.com/Ruslan-B/AR.Drone).

This projects is intended to be part of the Ruslans AR.Drone project when ready.

Thanks to [Yury Rozhdestvensky](https://github.com/yur) from [robodem.com](http://robodem.com) we got ability to create command chains

## Info Links

Video: http://youtu.be/7FKW6CPlDa8

XML example: http://db.stefs.me/BehaviorTreeOfVideo1.xml

## Dependencies

[FFmpeg.AutoGen](https://github.com/Ruslan-B/FFmpeg.AutoGen) - .NET wrapper for FFmpeg.

##License

Copyright 2013 Ruslan Balanukhin ruslan.balanukhin@gmail.com for the base project
Copyright 2015 Stefan Spasovski me@stefs.me for the behavior trees subproject, and it's usages in the base projects

GNU Lesser General Public License (LGPL) version 3 or later.  
http://www.gnu.org/licenses/lgpl.html

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.